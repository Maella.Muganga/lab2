package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{

    private ArrayList<FridgeItem> items;
    private final int maksKapasitet = 20;

    public Fridge() {
        items = new ArrayList<FridgeItem>();
    }

    @Override
    public int nItemsInFridge() {
        return items.size();
    }

    @Override
    public int totalSize() {
        return maksKapasitet;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (maksKapasitet - nItemsInFridge() > 0){
            items.add(item);
            return true;
        }else{
            return false;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (items.contains(item)) {
            items.remove(item);
        }
        else {
            throw new NoSuchElementException("No such" + item + "in Fridge");
        }
    }

    @Override
    public void emptyFridge() {
        items.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expired = new ArrayList<>();
        for (FridgeItem item: items) {
            if(item.hasExpired()) {
                expired.add(item);
            }
        }
        items.removeAll(expired);
        return expired;
    }
}
